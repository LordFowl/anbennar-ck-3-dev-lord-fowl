﻿zanite = {
	color = { 213 98 117 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

brasanni = {
	color = { 205 233 232 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		100 = arab
	}
}

gelkar = {
	color = { 54 184 198 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		25 = arab
		75 = mediterranean_byzantine
	}
}

bahari = {
	color = { 126 213 89 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		50 = arab
		50 = mediterranean_byzantine
	}
}


surani = {
	color = { 245 207 137 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}

sadnatu = {
	color = { 60 45 10 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}

masnsih = {
	color = { 200 105 50 }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}

maqeti = {
	color = { 190 190 50 }
	created = 930.1.1
	parents = { zanite }
	
	ethos = ethos_bureaucratic
	heritage = heritage_bulwari
	language = language_bulwari
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_hereditary_hierarchy
		tradition_chivalry
		tradition_castle_keepers
		tradition_longbow_competitions
	}
	
	name_list = name_list_bulwari
	
	coa_gfx = { arabic_group_coa_gfx }
	building_gfx = { mena_building_gfx }
	clothing_gfx = { mena_clothing_gfx dde_abbasid_clothing_gfx }
	unit_gfx = { mena_unit_gfx }
	
	ethnicities = {
		10 = arab
	}
}