﻿dynasty_silmuna = {
	name = "dynn_Silmuna"
	culture = "damerian"
}

dynasty_siloriel = {
	name = "dynn_Siloriel"
	culture = "lorentish"
}

dynasty_silgarion = {
	name = "dynn_Silgarion"
	culture = "damerian"
}

dynasty_silistra = {
	name = "dynn_Silistra"
	culture = "damerian"
}

dynasty_silurion = {
	name = "dynn_Silurion"
	culture = "damerian"
}

dynasty_silcalas = {
	name = "dynn_Silcalas"
	culture = "damerian"
}

dynasty_silebor = {
	name = "dynn_Silebor"
	culture = "damerian"
}

dynasty_silnara = {
	name = "dynn_Silnara"
	culture = "damerian"
}

dynasty_tretunis = {
	name = "dynn_Tretunis"
	culture = "tretunic"
}


9 = {
	name = "dynn_Pearlman"
	culture = "pearlsedger"
}

dynasty_roilsardis = {
	name = "dynn_Roilsardis"
	culture = "roilsardi"
}

11 = {
	prefix = "dynnp_sil"
	name = "dynn_Vis"
	culture = "hillfoot_halfling"
	motto = dynn_Vis_motto
}

12 = {
	prefix = "dynnp_sil"
	name = "dynn_Deranne"
	culture = "derannic"
}

dynasty_sorncost = {
	prefix = "dynnp_sil"
	name = "dynn_Sorncost"
	culture = "sorncosti"
}

14 = {
	name = "dynn_Lorentis"
	culture = "lorenti"
}

15 = {
	name = "dynn_Jaherzuir"
	culture = "sun_elvish"
}

16 = {
	name = "dynn_Redstone"
	culture = "ruby_dwarvish"
}

18 = {
	prefix = "dynnp_sil"
	name = "dynn_Uelaire"
	culture = "damerian"
}

19 = {
	name = "dynn_Roysfort"
	culture = "roysfoot_halfling"
}

20 = {
	name = "dynn_Iochand"
	culture = "creek_gnomish"
}

dynasty_vernid = {
	name = "dynn_Vernid"
	culture = "vernman"
}

dynasty_iacoban = {
	name = "dynn_Iacoban"
	culture = "corvurian"
}

22 = {
	name = "dynn_Coddorran"
	culture = "cliff_gnomish"
}

23 = {
	name = "dynn_Askeling"
	culture = "reverian"
}

dynasty_gawe = {
	name = "dynn_Gawe"
	culture = "gawedi"
}

25 = {
	name = "dynn_Eaglecrest"
	culture = "gawedi"
}

26 = {
	name = "dynn_Mooring"
	culture = "moorman"
}

dynasty_arthil = {
	name = "dynn_ta_arthil"
	culture = "moon_elvish"
}

28 = {
	name = "dynn_ta_galsheah"
	culture = "moon_elvish"
}

29 = {
	name = "dynn_Adshaw"
	culture = "old_alenic"
}

30 = {
	name = "dynn_Serpentsgard"
	culture = "blue_reachman"
}

31 = {
	name = "dynn_Cobbler"
	culture = "blue_reachman"
}

32 = {
	name = "dynn_Wex"
	culture = "wexonard"
}

33 = {
	name = "dynn_Sugambic"
	culture = "wexonard"
}

34 = {
	name = "dynn_Bisan"
	culture = "wexonard"
}

35 = {
	name = "dynn_Silverhammer"
	culture = "silver_dwarvish"
}

36 = {
	name = "dynn_Esmar"
	culture = "esmari"
}

37 = {
	name = "dynn_Bennon"
	culture = "esmari"
}

38 = {
	prefix = "dynnp_sil"
	name = "dynn_Estallen"
	culture = "ryalani"
}

39 = {
	name = "dynn_Ryalan"
	culture = "ryalani"
}

40 = {
	prefix = "dynnp_sil"
	name = "dynn_Leslinpar"
	culture = "esmari"
}

41 = {
	name = "dynn_Havoran"
	culture = "esmari"
}

dynasty_lunatein = {
	name = "dynn_talunetein"
	culture = "moon_elvish"
}

43 = {
	name = "dynn_Farran"
	culture = "esmari"
}

44 = {
	name = "dynn_Eldman"
	culture = "crownsman"
}

45 = {
	name = "dynn_Vrorensson"
	culture = "white_reachman"
}

dynasty_ebonfrost = {
	name = "dynn_Ebonfrost"
	culture = "black_castanorian"
}

47 = {
	name = "dynn_Aldwoud"
	culture = "white_reachman"
}

48 = {
	name = "dynn_Tederfremh"
	culture = "moon_elvish"
}

49 = {
	prefix = "dynnp_sil"
	name = "dynn_Cast"
	culture = "castanorian"
}

50 = {
	prefix = "dynnp_sil"
	name = "dynn_Anor"
	culture = "castanorian"
}

51 = {
	name = "dynn_Marr"
	culture = "marrodic"
}

52 = {
	name = "dynn_Balmire"
	culture = "old_alenic"
}

53 = {
	name = "dynn_Minesmiter"
	culture = "copper_dwarvish"
}

54 = {
	name = "dynn_Locke"
	culture = "oakfoot_halfling"
	motto = dynn_Locke_motto
}

55 = {
	prefix = "dynnp_szel"
	name = "dynn_Akalses"
	culture = "zanite"
}

56 = {
	prefix = "dynnp_szel"
	name = "dynn_Erubas"
	culture = "zanite"
}

57 = {
	name = "dynn_Esshyl"
	culture = "castanorian"
}

58 = {
	prefix = "dynnp_of"
	name = "dynn_Verteben"
	culture = "crownsman"
	motto = dynn_Verteben_motto
}

59 = {
	prefix = "dynnp_of"
	name = "dynn_Shieldrest"
	culture = "adeanic"
	motto = dynn_Shieldrest_motto
}

60 = {
	name = "dynn_Asrat"
	culture = "surani"
}

61 = {
	name = "dynn_Semiz"
	culture = "surani"
}

62 = {
	prefix = "dynnp_az"
	name = "dynn_Segh"
	culture = "citrine_dwarvish"
}

63 = {
	prefix = "dynnp_szel"
	name = "dynn_Gelkalis"
	culture = "gelkar"
}

64 = {
	prefix = "dynnp_szal"
	name = "dynn_Maqet"
	culture = "maqeti"
}

65 = {
	name = "dynn_Varaesa"
	culture = "moon_elvish"
}

66 = {
	prefix = "dynnp_of"
	name = "dynn_Escin"
	culture = "adeanic"
}

67 = {
	name = "dynn_Crodamos"
	culture = "kheteratan"
}

68 = {
	name = "dynn_Deshakos"
	culture = "deshaki"
}

69 = {
	name = "dynn_Keskhasa"
	culture = "khasani"
}

#100 - 200 : Jay
100 = {
	prefix = "dynnp_of"
	name = "dynn_Oldhaven"
	culture = "marcher"
}

dynasty_aubergentis = {
	name = "dynn_Aubergentis"
	culture = "lorenti"
}

dynasty_sidericis = {
	name = "dynn_Sidericis"
	culture = "damerian"
}

dynasty_acromis = {
	name = "dynn_Acromis"
	culture = "old_damerian"
}

dynasty_cymlan = {
	name = "dynn_Cymlan"
	culture = "moon_elvish"
}

dynasty_magehand = {	#Serondal Magehand
	name = "dynn_Magehand"
	culture = "moon_elvish"
}

dynasty_blacktower = {	#Serondal Magehand
	name = "dynn_Blacktower"
	culture = "vertesker"
}

dynasty_longlance = {	#of Caylen Longlance fame
	name = "dynn_Longlance"
	culture = "adeanic"
}

dynasty_drakesford = {
	name = "dynn_Drakesford"
	culture = "gawedi"
}

dynasty_gladeguard = {
	name = "dynn_Gladeguard"
	culture = "moon_elvish"
}

dynasty_plumwall = {
	name = "dynn_Plumwall"
	culture = "old_damerian"
}

dynasty_cliffman = {
	name = "dynn_Cliffman"
	culture = "lenco_damerian"
}

dynasty_taxwick = {
	name = "dynn_Taxwick"
	culture = "old_damerian"
}

dynasty_exwes = {
	name = "dynn_Exwes"
	culture = "exwesser"
}

dynasty_sarfort = {
	name = "dynn_Sarfort"
	culture = "castanorian"
	motto = dynn_Sarfort_motto
}

dynasty_karnid = {
	name = "dynn_Karnid"
	culture = "korbarid"
	motto = dynn_Karnid_motto
}

dynasty_denarzuir = {
	name = "dynn_Denarzuir"
	culture = "sun_elvish"
	motto = dynn_Denarzuir_motto
}

dynasty_tiferben = {
	name = "dynn_Tiferben"
	culture = "corvurian"
	motto = dynn_Tiferben_motto
}

dynasty_fiachlar = {
	prefix = "dynnp_sil"
	name = "dynn_Fiachlar"
	culture = "corvurian"
	motto = dynn_sil_Fiachlar_motto
}

dynasty_magda = {
	prefix = "dynnp_sil"
	name = "dynn_Magda"
	culture = "wexonard"
}

dynasty_aelvar = {
	prefix = "dynnp_sil"
	name = "dynn_Aelvar"
	culture = "derannic"
	motto = dynn_sil_Aelvar_motto
}

dynasty_colson = {
	name = "dynn_Colson"
	culture = "derannic"
	motto = dynn_sil_Aelvar_motto
}

dynasty_ording = {
	prefix = "dynnp_of"
	name = "dynn_Ording"
	culture = "lorenti"
	motto = dynn_of_Ording_motto
}

dynasty_ventis = {
	name = "dynn_ventis"
	culture = "lorenti"
	motto = dynn_Ventis_motto
}

dynasty_duskwatcher = {
	name = "dynn_Duskwatcher"
	culture = moon_elvish
	motto = dynn_Duskwatcher_motto
}

dynasty_caylentis = {
	name = "dynn_Caylentis"
	culture = lorenti
}

dynasty_nisabat = {
	name = "dynn_Nisabat"
	culture = bahari
}

dynasty_casthil = {
	prefix = "dynnp_sil"
	name = "dynn_Casthil"
	culture = "lorentish"
	motto = dynn_sil_Casthil_motto
}

dynasty_balgard = {
	name = "dynn_balgard"
	culture = old_alenic
}

dynasty_kobali = {
	name = "dynn_Kobali"
	culture = "milcori"
	# motto = dynn_Kobali_motto
}

dynasty_aldegarde = {
	name = "dynn_Aldegarde"
	culture = "lorentish"
	motto = dynn_Aldegarde_motto
}

dynasty_arthelis = {
	name = "dynn_Arthelis"
	culture = "lorenti"
}

dynasty_brannis = {
	name = "dynn_Brannis"
	culture = "lorenti"
}

dynasty_medrontis = {
	name = "dynn_Medrontis"
	culture = "lorenti"
}

dynasty_calonis = {
	name = "dynn_Calonis"
	culture = "lorenti"
}

dynasty_gwevoris = {
	name = "dynn_Gwevoris"
	culture = "lorenti"
}

dynasty_derancestir = {
	prefix = "dynnp_sil"
	name = "dynn_Derancestir"
	culture = "damerian"
}

dynasty_findhan = {
	name = "dynn_Findhan"
	culture = "lorenti"
}

dynasty_ambura = {
	name = "dynn_Ambura"
	culture = "lorenti"
}

dynasty_clobronh = {
	name = "dynn_Clobronh"
	culture = "lorenti"
}

dynasty_sigvardsson = {
	name = "dynn_sigvardsson"
	culture = "dalric"
}

dynasty_goldeneyes = {
	name = "dynn_goldeneyes"
	culture = "old_alenic"
}

dynasty_frostwall = {
	name = "dynn_frostwall"
	culture = "old_alenic"
}

dynasty_bjarnsson = {
	name = "dynn_bjarnsson"
	culture = "dalric"
}

dynasty_sidaett = {
	name = "dynn_sidaett"
	culture = "dalric"
}

dynasty_bjarnsson = {
	name = "dynn_bjarnsson"
	culture = "dalric"
}

dynasty_sidaett = {
	name = "dynn_sidaett"
	culture = "dalric"
}

dynasty_ionnidar = {
	prefix = "dynnp_sil"
	name = "dynn_Ionnidar"
	culture = "lorentish"	
}

dynasty_ionnidar = {
	prefix = "dynnp_sil"
	name = "dynn_Ionnidar"
	culture = "lorentish"	
}

dynasty_garmonis = {
	name = "dynn_Garmonis"
	culture = "lorenti"	
}

dynasty_harascilde = {
	prefix = "dynnp_sil"
	name = "dynn_Harascilde"
	culture = "lorentish"	
}
dynasty_drostening = {
	name = "dynn_Drostening"
	culture = "black_castanorian"	
}
dynasty_ottocam = {
	name = "dynn_Ottocam"
	culture = "wexonard"	
}
dynasty_singkeep = {
	name = "dynn_Singkeep"
	culture = "moon_elvish"
}
dynasty_marelis = {
	name = "dynn_Marelis"
	culture = "esmari"
}

dynasty_nurael = {
	name = "dynn_nurael"
	culture = "moon_elvish"	
}

dynasty_larthan = {
	name = "dynn_larthan"
	culture = "moon_elvish"	
}

dynasty_silcarod = {
	name = "dynn_silcarod"
	culture = "moon_elvish"	
}

dynasty_seawatcher = {
	name = "dynn_seawatcher"
	culture = moon_elvish
}

dynasty_truesight = {
	name = "dynn_truesight"
	culture = moon_elvish
}

dynasty_toarnen = {
	name = "dynn_toarnen"
	culture = roilsardi
}

dynasty_cronesford = {
	name = "dynn_cronesford"
	culture = milcori
}

dynasty_celliande = {
	name = "dynn_celliande"
	culture = roilsardi
}

dynasty_arannen = {
	prefix = "dynnp_sil"
	name = "dynn_arannen"
	culture = roilsardi
}

dynasty_endersby = {
	name = "dynn_endersby"
	culture = milcori
}

dynasty_wesdam = {
	prefix = "dynnp_sil"
	name = "dynn_wesdam"
	culture = lenco_damerian
}

dynasty_wispsiren = {
	name = "dynn_wispsiren"
	culture = milcori
}

dynasty_nurcedor = {
	name = "dynn_nurcedor"
	culture = businori
}

dynasty_wayguard = {
	name = "dynn_wayguard"
	culture = marcher
}

dynasty_beldarbronhd = {
	name = "dynn_beldarbronhd"
	culture = businori
}

dynasty_devaced = {
	name = "dynn_devaced"
	culture = marcher
}

dynasty_gabalaire = {
	name = "dynn_gabalaire"
	culture = damerian
}

dynasty_acengard = {
	name = "dynn_acengard"
	culture = adeanic
	prefix = "dynnp_of"
}

dynasty_aldenmore = {
	prefix = "dynnp_of"
	name = "dynn_aldenmore"
	culture = castanorian
}

dynasty_ryalfeld = {
	prefix = "dynnp_of"
	name = "dynn_ryalfeld"
	culture = castanorian
}